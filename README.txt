F# script that helps decrypt a "multiple time pad", i.e.: when a single pad is used to encrypt different plaintexts.
To run:
	* Load labMain.fsx into fsharp interactive
	* Call decrypt function with path to ciphertext file
	* Ciphers are expected to be hex-encoded ascii strings as in "ct4File.txt"