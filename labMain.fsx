﻿module labMain

open System
open System.Text
open System.IO
open System.Linq
open System.Collections.Generic

//ciphers file: @"C:\Users\alpha\dev\FSharpScripts\comp_sec_lab1\ctFile_4.txt"

/// read hex encoded ciphers from a txt file, return ascii encoded strings
let readHexCiphersFromFile (file:string) =
    use sr = new StreamReader(file)
    let hexToByteArr (str:string) =
        [| for i in [0 .. 2 .. str.Length - 2] do
               yield Byte.Parse(str.[i .. i+1], Globalization.NumberStyles.HexNumber) |]    
    sr.ReadToEnd().Trim().Split([|'\r'; '\n'; ' '|], StringSplitOptions.RemoveEmptyEntries)
    |> Array.map hexToByteArr
    |> Array.map (new ASCIIEncoding()).GetString


/// partition ciphers: one cipher to xor them all!
let partitionCiphers ciphers =
    match ciphers with
    | [||] -> String.Empty, [||]
    | [|c|]  -> c, [||]
    | _ -> ciphers.[(ciphers.Length - 1)], ciphers.[0 .. (ciphers.Length - 2)]


/// xor 2 strings of equal length -> to string
let strXor (str1:string) (str2:string) =
    try
        let ascii = new ASCIIEncoding()
        Array.map2 (fun e1 e2 -> e1 ^^^ e2) (ascii.GetBytes(str1)) (ascii.GetBytes(str2))
        |> ascii.GetString
    with
        | :? System.ArgumentException as ex -> failwith ex.Message



/// build set of xors: prim is the cipher we want to decript;
///     sec is a string array containing the other ciphers
let buildXorSet prim sec =
    Array.map (strXor prim) sec


/// determine if xor of 2 chars is [A-Z, a-z, _]
let inline isXorValid i c (str:string) =
    let xor = (byte c) ^^^ (byte str.[i]) |> char
    match xor with
    | ' ' -> true
    | x when xor >= 'a' && xor <= 'z' -> true
    | X when xor >= 'A' && xor <= 'Z' -> true
    | _ -> false



/// iterate thru set of xors, "guess" valid plaintext chars
let decrypt cipherFile =
    let validAscii = [ [|'a' .. 'z'|]; [|'A' .. 'Z'|]; [|' '|] ] |> Array.concat
    let ciphers = readHexCiphersFromFile cipherFile
    let cipherPermutations = [| for i in 0 .. (-1 + ciphers.Length) -> 
                                    Array.permute (fun index -> (index + i) % (ciphers.Length) ) ciphers |]
    let solutionList = ResizeArray<ResizeArray<char>[]>()
    for permutation in cipherPermutations do
        let target, C = partitionCiphers permutation
        let xors = buildXorSet target C
        let plainText = Array.init (target.Length) (fun _ -> new ResizeArray<char>()) 
        for col = 0 to (plainText.Length - 1) do   // iterate cols
            let mutable i = 0
            let mutable solFound = false
            while (i < validAscii.Length) do
                solFound <- Array.forall (isXorValid col validAscii.[i]) xors
                if solFound then
                    plainText.[col].Add(validAscii.[i])
                i <- (i + 1)
        solutionList.Add(plainText)
    solutionList |> Array.ofSeq |> Array.rev

[<EntryPoint>]
let main _ =
    let sols = decrypt @"C:\Users\alpha\dev\FSharpScripts\comp_sec_lab1\ctFile_4.txt"
    //let sols = decrypt @"c:\users\alpha\desktop\ctFile_10.txt"
    for sol in sols do
        for str in sol do
            if str.Count = 1 then printf "%c" (str.[0])
            else printf "%c" '/'
        printfn "\n"
    Console.ReadKey(true) |> ignore
    0

